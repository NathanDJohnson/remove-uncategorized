<?php
/**
 * Plugin Name: Remove Uncategorized
 * Description: Removesthe uncategorized category if there's another category.
 * Author: Nathan Johnson
 * Licence: GPL2+
 * Licence URI: https://www.gnu.org/licenses/gpl-2.0.en.html
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

register_activation_hook( __FILE__ , 'wpse_106269_activation' );

function wpse_106269_activation() {
  $args = array(
    'posts_per_page'   => -1,
    'offset'           =>  0,
    'category'         => get_option( 'default_category' ),
    'post_status'      => 'any',
    'suppress_filters' => true,
  );
  $posts = get_posts( $args );
  foreach( $posts as $post ) {
    wpse_106269_maybe_remove_uncategorized_category( $post->ID );
  }
}

add_action( 'save_post', 'wpse_106269_save_post', 10, 3 );

function wpse_106269_save_post( $id, $post, $update ) {
  remove_action( 'save_post', 'wpse_106269_save_post', 10, 3 );
  wpse_106269_maybe_remove_uncategorized_category( $id );
  add_action( 'save_post', 'wpse_106269_save_post', 10, 3 );
}

function wpse_106269_maybe_remove_uncategorized_category( $id ) {
  $categories = get_the_category( $id );
  $default = get_cat_name( get_option( 'default_category' ) );
  if( count( $categories ) >= 2 && in_category( $default, $id ) ) {
    wp_remove_object_terms( $id, $default, 'category' );
  }
}
